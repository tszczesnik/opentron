# metadata
metadata = {
    'protocolName': 'Phusion PCR',
    'author': 'Tomasz Szczesnik <tomasz.szczesnik@bsse.ethz.ch>',
    'apiLevel': '2.6'}

def run(protocol):

    tiprack20 = protocol.load_labware('opentrons_96_tiprack_20ul', '3', '20ul tiprack')
    p20 = protocol.load_instrument('p20_single_gen2', 'left', tip_racks = [tiprack20])

    # put some ice underneath for the phusion mastermix
    tubeblock = protocol.load_labware('opentrons_24_tuberack_eppendorf_1.5ml_safelock_snapcap', '1')

    thermocycler = protocol.load_module('thermocycler')
    # should have a custom labware definition for pcr strips instead of using this nest wellplate.
    # should be ok since the xy positions and base are the same.
    thermocycler_tubes = thermocycler.load_labware('nest_96_wellplate_100ul_pcr_full_skirt', 'thermocycler plate')

    if thermocycler.lid_position != 'open':
        thermocycler.open_lid()

    thermocycler.set_block_temperature(4)

    # what to pipette where
    water_tube = 'A1'
    phusion_mastermix = 'A2'
    moclo_mastermix = 'A3'

    # range is inclusive of the first index, exclusive of the second
    # so 1..8 is range(1, 9)
    pcr_targets =   ["C" + str(i) for i in range(3, 10)]
    # use these as alternate pcr wells
    pcr_dilutions = ["D" + str(i) for i in range(3, 10)]
    moclo_targets = ["E" + str(i) for i in range(3, 10)]

    template_primers = [
        (['A4'], ['B3', 'B4']),
        (['A5'], ['B5', 'B6']),
        (['A6'], ['C1', 'C2']),
        (['B1'], ['C3', 'C4']),
        (['B1'], ['C5', 'C6']),
        (['B2'], ['D1', 'D2']),
        (['B2'], ['D3', 'D4'])]

    template_sources = [i for (i, j) in template_primers]
    primer_sources = [j for (i, j) in template_primers]

    # add water
    p20.transfer(
        7 * 3,
        tubeblock[water_tube],
        [thermocycler_tubes[well_name] for well_name in pcr_targets],
        # trash = False, 
        new_tip = 'once')

    # add template and primers
    for i in range(0, len(pcr_targets)):

        p20.transfer(
            1 * 3,
            [tubeblock.wells_by_name()[well_name] for well_name in template_sources[i]],
            thermocycler_tubes[pcr_targets[i]],
            # trash = False, 
            new_tip = 'always')

        p20.transfer(
            1 * 3,
            [tubeblock.wells_by_name()[well_name] for well_name in primer_sources[i]],
            thermocycler_tubes[pcr_targets[i]],
            # trash = False, 
            new_tip = 'always')

    # # add the master mix and mix
    # p20.transfer(
    #     10 * 3,
    #     tubeblock[phusion_mastermix],
    #     [thermocycler_tubes[well_name] for well_name in pcr_targets],
    #     # mix_after = (10, 20),
    #     # trash = False, 
    #     new_tip = 'always')

    for i in range(0, len(pcr_targets)):

        # transferring 30ul but want to keep the tip at the end.
        p20.pick_up_tip()
        p20.transfer(
            15,
            tubeblock[phusion_mastermix],
            thermocycler_tubes[pcr_targets[i]],
            new_tip = 'never')

        p20.drop_tip()
        p20.pick_up_tip()

        p20.transfer(
            15,
            tubeblock[phusion_mastermix],
            thermocycler_tubes[pcr_targets[i]],
            new_tip = 'never')

        p20.mix(10, 20, rate = 4.0)

        p20.transfer(
            20,
            thermocycler_tubes[pcr_targets[i]],
            thermocycler_tubes[pcr_dilutions[i]],
            new_tip = 'never')
        p20.transfer(
            20,
            thermocycler_tubes[pcr_targets[i]],
            thermocycler_tubes[moclo_targets[i]],
            new_tip = 'never')

        p20.drop_tip()

    protocol.pause('move some pcrs to the other machines')

    # run the thermocycler
    if thermocycler.lid_position != 'closed':
        thermocycler.close_lid()
    thermocycler.set_lid_temperature(105)

    profile = [
        {'temperature': 98, 'hold_time_seconds': 1},
        {'temperature': 60, 'hold_time_seconds': 5},
        {'temperature': 72, 'hold_time_seconds': 60}] # 15s per kb

    thermocycler.set_block_temperature(98, hold_time_seconds = 10, block_max_volume = 20)
    thermocycler.execute_profile(steps=profile, repetitions = 30, block_max_volume = 20)
    thermocycler.set_block_temperature(72, hold_time_seconds = 180, block_max_volume = 20)
    thermocycler.set_block_temperature(4)

    thermocycler.deactivate_lid()

    # thermocycler.open_lid()

    # protocol.pause('make sure the moclo master mix is there')

    # p20.transfer(
    #     19,
    #     tubeblock[water_tube],
    #     [thermocycler_tubes[well_name] for well_name in pcr_dilutions],
    #     # trash = False, 
    #     new_tip = 'once')


    # p20.transfer(
    #     9.5,
    #     tubeblock[moclo_mastermix],
    #     [thermocycler_tubes[well_name] for well_name in moclo_targets],
    #     # trash = False, 
    #     new_tip = 'once')

    # # dilute the pcr and add it to the moclo wells
    # for i in range(0, len(pcr_targets)):

    #     p20.pick_up_tip()
    #     p20.transfer(
    #         1,
    #         thermocycler_tubes[pcr_targets[i]],
    #         thermocycler_tubes[pcr_dilutions[i]],
    #         # trash = False, 
    #         mix_before = (3, 20),
    #         mix_after = (3, 20),
    #         new_tip = 'never')

    #     p20.transfer(
    #         0.5,
    #         thermocycler_tubes[pcr_dilutions[i]],
    #         thermocycler_tubes[moclo_targets[i]],
    #         # trash = False, 
    #         mix_after = (3, 10),
    #         new_tip = 'never')
    #     # p20.return_tip()
    #     p20.drop_tip()

    # # run the thermocycler
    # if thermocycler.lid_position != 'closed':
    #     thermocycler.close_lid()
    # thermocycler.set_lid_temperature(105)

    # profile = [
    #     {'temperature': 37, 'hold_time_minutes': 2},
    #     {'temperature': 16, 'hold_time_minutes': 5}]

    # thermocycler.execute_profile(steps=profile, repetitions = 30, block_max_volume = 20)
    # thermocycler.set_block_temperature(37, hold_time_minutes = 10, block_max_volume = 20)
    # thermocycler.set_block_temperature(80, hold_time_minutes = 10, block_max_volume = 20)
    # thermocycler.set_block_temperature(4)
    # thermocycler.deactivate_lid()
