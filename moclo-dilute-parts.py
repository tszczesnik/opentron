metadata = {
    'protocolName': 'MoClo Parts',
    'author': 'Tomasz Szczesnik <tomasz.szczesnik@bsse.ethz.ch>',
    'apiLevel': '2.6'}

def run(protocol):

    tiprack20 = protocol.load_labware('opentrons_96_tiprack_20ul', '3', '20ul tiprack')
    p20 = protocol.load_instrument('p20_single_gen2', 'left', tip_racks = [tiprack20])

    # put some ice underneath for the phusion mastermix
    tubeblock = protocol.load_labware('opentrons_24_tuberack_eppendorf_1.5ml_safelock_snapcap', '1')

    thermocycler = protocol.load_module('thermocycler')
    # should have a custom labware definition for pcr strips instead of using this nest wellplate.
    # should be ok since the xy positions and base are the same.
    thermocycler_tubes = thermocycler.load_labware('nest_96_wellplate_100ul_pcr_full_skirt', 'thermocycler plate')

    if thermocycler.lid_position != 'open':
        thermocycler.open_lid()

    thermocycler.set_block_temperature(4)

    # what to pipette where
    water_tube = 'A1'
    moclo_mastermix = 'A2'


    # note: range is inclusive of first index and exclusive of second
    # goddamn python
    pcr_initial =   ["C" + str(i) for i in range(3, 9)]
    pcr_dilutions = ["D" + str(i) for i in range(3, 9)]
    moclo_targets = ["E" + str(i) for i in range(3, 9)]

    p20.transfer(
        19,
        tubeblock[water_tube],
        [thermocycler_tubes[well_name] for well_name in pcr_dilutions],
        # trash = False, 
        new_tip = 'once')

    p20.transfer(
        9.5,
        tubeblock[moclo_mastermix],
        [thermocycler_tubes[well_name] for well_name in moclo_targets],
        # trash = False, 
        new_tip = 'once')

    # dilute the pcr and add it to the moclo wells
    for i in range(0, len(pcr_initial)):

        p20.pick_up_tip()
        p20.transfer(
            1,
            thermocycler_tubes[pcr_initial[i]],
            thermocycler_tubes[pcr_dilutions[i]],
            mix_before = (3, 20),
            new_tip = 'never')

        p20.mix(10, 20, rate = 4.0)

        p20.transfer(
            0.5,
            thermocycler_tubes[pcr_dilutions[i]],
            thermocycler_tubes[moclo_targets[i]],
            new_tip = 'never')

        p20.mix(10, 20, rate = 4.0)

        p20.drop_tip()

    # run the thermocycler
    if thermocycler.lid_position != 'closed':
        thermocycler.close_lid()
    thermocycler.set_lid_temperature(105)

    profile = [
        {'temperature': 37, 'hold_time_minutes': 2},
        {'temperature': 16, 'hold_time_minutes': 5}]

    thermocycler.execute_profile(steps=profile, repetitions = 30, block_max_volume = 20)
    # thermocycler.set_block_temperature(37, hold_time_minutes = 10, block_max_volume = 20)
    # thermocycler.set_block_temperature(80, hold_time_minutes = 10, block_max_volume = 20)

    thermocycler.set_block_temperature(4)
    # thermocycler.deactivate_lid()
