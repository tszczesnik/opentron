metadata = {
    'protocolName': 'MoClo',
    'author': 'Tomasz Szczesnik <tomasz.szczesnik@bsse.ethz.ch>',
    'apiLevel': '2.6'}

def run(protocol):

    tiprack20 = protocol.load_labware('opentrons_96_tiprack_20ul', '9', '20µl tiprack')
    p20 = protocol.load_instrument('p20_single_gen2', 'left', tip_racks = [tiprack20])

    tubeblock = protocol.load_labware('opentrons_24_tuberack_eppendorf_1.5ml_safelock_snapcap', '3')


    thermocycler = protocol.load_module('thermocycler')
    # should have a custom labware definition for pcr strips instead of using this nest wellplate.
    # should be ok since the xy positions and base are the same.
    thermocycler_tubes = thermocycler.load_labware('nest_96_wellplate_100ul_pcr_full_skirt', 'thermocycler plate')

    if thermocycler.lid_position != 'open':
        thermocycler.open_lid()

    thermocycler.set_block_temperature(4)
    protocol.pause('check that all tubes are in the correct place')

    # what to pipette where

    moclo_target_source = [
        ('A1', ['B3', 'B1']),
        ('A2', ['B4', 'B1']),
        ('A3', ['B5', 'B1']),
        ('A4', ['B6', 'B1']),
        ('A5', ['B3', 'B2']),
        ('A6', ['B4', 'B2']),
        ('A7', ['B5', 'B2']),
        ('A8', ['B6', 'B2'])]

    moclo_mastermix = thermocycler_tubes['H12']

    water_tube = tubeblock['A1']

    # add the master mix first.
    # need a large volume first to reliably transfer the smaller parts.
    p20.transfer(
        5,
        moclo_mastermix,
        [thermocycler_tubes[i] for i, j in moclo_target_source],
        new_tip = 'once')

    # add each of the moclo components
    for i, j in moclo_target_source:

        p20.transfer(
            0.5,
            [tubeblock.wells_by_name()[well_name] for well_name in j],
            thermocycler_tubes[i],
            new_tip = 'always')

        p20.transfer(
            (5 - (len(j)/2)),
            water_tube,
            thermocycler_tubes[i],
            mix_after = (3, 10),
            new_tip = 'always')

    # run the thermocycler

    if thermocycler.lid_position != 'closed':
        thermocycler.close_lid()
    thermocycler.set_lid_temperature(105)

    # warning: setting consecutive cycles to the same temperature will result in an error when the module tries to check the temperature has been reached (for now).
    profile = [
        {'temperature': 37, 'hold_time_minutes': 5},
        {'temperature': 16, 'hold_time_minutes': 5}]

    thermocycler.execute_profile(steps=profile, repetitions = 30, block_max_volume = 20)
    thermocycler.set_block_temperature(37, hold_time_minutes = 10, block_max_volume = 20)
    thermocycler.set_block_temperature(80, hold_time_minutes = 10, block_max_volume = 20)
    thermocycler.set_block_temperature(4)
    thermocycler.deactivate_lid()

