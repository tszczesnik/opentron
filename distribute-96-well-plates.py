import math
from opentrons import types

# metadata = {
#     'protocolName': 'Distribute to 96 well plates',
#     'author': 'Tomasz Szczesnik <tomasz.szczesnik@bsse.ethz.ch>',
#     'apiLevel': '2.6'
# }

import opentrons.execute
protocol = opentrons.execute.get_protocol_api('2.9')
protocol.home()

# def run(protocol):

tiprack300 =  protocol.load_labware('opentrons_96_tiprack_300ul', '3', '300ul tiprack')
p300 = protocol.load_instrument('p300_multi_gen2', 'right', tip_racks = [tiprack300])

# just the cap of filter P1000 tip box 
# waste = protocol.load_labware('agilent_1_reservoir_290ml', '1', 'liquid waste').wells()[0].top()
reservoir = protocol.load_labware('nest_12_reservoir_15ml', '2', 'reagent reservoir')

# put them from '1' in the grid...
number_of_plates = 1

# technically not this plate, but whatever.
# note: range is inclusive of first position, exclusive of last
# + 48 to get to the actual character encoding for that integer.
well_plates = [protocol.load_labware('nest_96_wellplate_200ul_flat', str(i)) for i in range(1, 1 + number_of_plates)]


p300.pick_up_tip()
for w in range(0, number_of_plates):
    # p300.transfer(
    p300.distribute(
        40,
        reservoir.wells()[w],
        # probably an easier way to specify all columns.
        [well_plates[w].columns_by_name()[str(i)] for i in range(1, 13)],
        new_tip = 'never',
        trash = False)
p300.drop_tip()
# p300.return_tip()

