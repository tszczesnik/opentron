import math
from opentrons import types

metadata = {
    'protocolName': 'Zyppy MagBead Miniprep',
    'author': 'Tomasz Szczesnik <tomasz.szczesnik@bsse.ethz.ch>',
    'apiLevel': '2.6'
}

def run(protocol):

    tiprack300 =  protocol.load_labware('opentrons_96_tiprack_300ul', '3', '300ul tiprack')
    p300 = protocol.load_instrument('p300_multi_gen2', 'right', tip_racks = [tiprack300])

    tiprack20 =  protocol.load_labware('opentrons_96_tiprack_20ul', '9', '20ul tiprack')
    p20 = protocol.load_instrument('p20_single_gen2', 'left', tip_racks = [tiprack20])

    magdeck = protocol.load_module('magnetic module gen2', '1')
    # actually the zymo plate, check if it treats it similarly enough
    # switched to the actual nest plate now.
    magdeck_plate = magdeck.load_labware('nest_96_wellplate_2ml_deep', 'deepwell plate on magdeck')

    # this should be incorporated into the labware definition
    magdeck_default_height = 6.6
    magdeck.disengage()

    # no labware on the tempdeck since it is only to have things placed on top
    tempdeck = protocol.load_module('temperature module gen2', '10')

    # # technically not this plate, but whatever.
    # collection_plate = protocol.load_labware('nest_96_wellplate_2ml_deep', '7')

    # only running a few samples now and want to transfer to collection tubes.
    elution_tubes = protocol.load_labware('opentrons_24_tuberack_eppendorf_1.5ml_safelock_snapcap', '11')

    reservoir = protocol.load_labware('nest_12_reservoir_15ml', '2', 'reagent reservoir')

    # just the cap of filter P1000 tip box 
    waste = protocol.load_labware('agilent_1_reservoir_290ml', '4', 'liquid waste').wells()[0].top()

    # what is in the resorvoir
    lysis_buffer = reservoir.wells()[0]
    neutralisation_buffer = reservoir.wells()[1]
    magclear_beads = reservoir.wells()[2]
    magbind_beads = reservoir.wells()[3]
    endowash_buffer = reservoir.wells()[4]
    wash_buffer = reservoir.wells()[5]
    elution_buffer = reservoir.wells()[6]

    # sample setup
    # note: zero indexing for column numbers
    # start_col = 2, end_col = 3 means that only the 3rd column is used.
    start_col = 0
    num_cols = 1

    bacteria_cols = magdeck_plate.rows()[0][start_col : start_col + num_cols]
    binding_cols = magdeck_plate.rows()[0][start_col + num_cols : start_col + 2 * num_cols]
    
    # useful functions

    # for some reason next_tip is specified for individual tip racks
    # rather than for a pipette and its linked tip racks
    def next_tip_and_rack(pipette):
        res = list(filter(None, (map(lambda x : x.next_tip(), pipette.tip_racks))))
        return(res[0])

    # run the actual protocol
    for m in bacteria_cols:
        p300.pick_up_tip()
        p300.transfer(
            100,
            lysis_buffer,
            m,
            new_tip = 'never',
            trash = False)
        for _ in range(5):
            p300.aspirate(300, m.bottom(10), rate = 1.5)
            p300.dispense(300, m.bottom(0.2), rate = 1.5)
        p300.blow_out(m.top(-5))
        p300.drop_tip()
    
    protocol.delay(minutes = 5)
    # protocol.pause('lysis wait finished, check everything is ok')

    for m in bacteria_cols:
        p300.pick_up_tip()
        p300.transfer(
            450,
            neutralisation_buffer,
            m.top(),
            new_tip = 'never',
            trash = False)
        for _ in range(10):
            p300.aspirate(300, m.bottom(15), rate = 1.5)
            p300.dispense(300, m.bottom(0.2), rate = 1.5)
        p300.blow_out(m.top(-5))
        p300.drop_tip()

    # protocol.pause('neutralisation finished (no wait, just mix), check everything is ok. should be dark yellow.')

    # mix the beads before transferring
    p300.pick_up_tip()
    for _ in range(10):
        p300.aspirate(100, magclear_beads.bottom(0.5), rate = 2.0)
        p300.dispense(100, magclear_beads.bottom(20), rate = 2.0)
    p300.blow_out(magclear_beads.top(-5))

    for m in bacteria_cols:
        if not p300.hw_pipette['has_tip']:
            p300.pick_up_tip()
        p300.transfer(50, magclear_beads, m, new_tip = 'never')
        for _ in range(5):
            p300.aspirate(300, m.bottom(18), rate = 2.0)
            p300.dispense(300, m.bottom(0.2), rate = 2.0)
        p300.blow_out(m.top(-5))
        p300.drop_tip()

    magdeck.engage(height = magdeck_default_height)

    protocol.delay(minutes = 7)
    protocol.pause('magbead clearing finished, check everything is ok')

    # check that this goes per column!
    # want to do this from about half the way down in the well.
    # check what the current position is! particularly relative to the precipitate and such.
    for (a, b) in zip(bacteria_cols, binding_cols):
        p300.transfer(
            750,
            a.bottom(5),
            b,
            new_tip = 'once',
            trash = True)

    magdeck.disengage()

    # protocol.pause('move the collection plate to the magdeck!')

    # again mix the beads before transferring
    p300.pick_up_tip()
    for _ in range(10):
        p300.aspirate(100, magbind_beads.bottom(0.5), rate = 2.0)
        p300.dispense(100, magbind_beads.bottom(20), rate = 2.0)
    p300.blow_out(magbind_beads.top(-5))
    # distribute to all wells to avoid the beads settling while mixing
    p300.distribute(30, magbind_beads, binding_cols, new_tip = 'never')
    p300.drop_tip()

    wash_tips = [None] * num_cols

    for i in range(0, num_cols):
        m = binding_cols[i]
        wash_tips[i] = next_tip_and_rack(p300)
        p300.pick_up_tip()
        for _ in range(5):
            p300.aspirate(300, m.bottom(10), rate = 4.0)
            p300.dispense(300, m.bottom(0.2), rate = 4.0)
        p300.blow_out(m.top(-5))
        p300.return_tip()

    for _ in range(20):
        protocol.delay(seconds = 30)
        for m, t in zip(binding_cols, wash_tips):
            p300.pick_up_tip(t)
            p300.aspirate(300, m.bottom(10), rate = 4.0)
            p300.dispense(300, m.bottom(0.2), rate = 4.0)
            p300.return_tip(t)

    magdeck.engage(height = magdeck_default_height)
    protocol.delay(minutes = 7)
    protocol.pause('magbead binding finished, check everything is ok')

    # check that this doesnt disrupt the beads!
    for m, t in zip(binding_cols, wash_tips):
        p300.pick_up_tip(t)
        p300.transfer(750, m.bottom(0.2), waste, new_tip = 'never', trash = False)
        p300.drop_tip()

    magdeck.disengage()

    # to mix the wells in the position closer to the magnets
    angles_m = [0.85 if (i + start_col) % 2 == 0 else -0.85 for i, col in enumerate(binding_cols)]
    mix_locs_m = [col.bottom().move(types.Point(x=angle, y=0, z=0.8)) for col, angle in zip(binding_cols, angles_m)]

    protocol.pause("refill tip racks")
    p300.reset_tipracks()

    # want to switch both washes (endowash and ethanol wash) to save the tips between first pipette then discarding the waste
    # protocol says 30s vortex to resuspend then 2 min wait
    for m, mix_loc in zip(binding_cols, mix_locs_m):
        p300.pick_up_tip()
        p300.transfer(
            200,
            endowash_buffer,
            m,
            new_tip = 'never',
            trash = False)
        # for _ in range(10):
        #     p300.aspirate(200, m.bottom(0.2))
        #     p300.dispense(200, m.bottom(2))
        p300.move_to(m.bottom(0.2))
        p300.mix(10, 200, mix_loc, rate = 4.0)
        p300.blow_out(m.top())
        p300.drop_tip()

    magdeck.engage(height = magdeck_default_height)
    protocol.delay(minutes = 7)
    # protocol.pause('endowash finished, check everything is ok')
    for m in binding_cols:
        p300.pick_up_tip()
        p300.transfer(200, m.bottom(0.2), waste, new_tip = 'never', trash = False)
        p300.drop_tip()
    magdeck.disengage()

    # same as for endowash: 30s vortex 2 min wait.
    for _ in range(2):
        for m, mix_loc in zip(binding_cols, mix_locs_m):
            p300.pick_up_tip()
            p300.transfer(
                400,
                wash_buffer,
                m.top(),
                new_tip = 'never',
                trash = False)
            # for _ in range(10):
            #     p300.aspirate(250, m.bottom(0.2))
            #     p300.dispense(250, m.bottom(2))
            p300.move_to(m.bottom(0.2))
            p300.mix(10, 250, mix_loc, rate = 4.0)
            p300.blow_out(m.top())
            p300.drop_tip()

        magdeck.engage(height = magdeck_default_height)
        protocol.delay(minutes = 7)
        # protocol.pause('wash finished, check everything is ok')
        for m in binding_cols:
            p300.pick_up_tip()
            p300.transfer(400, m.bottom(0.2), waste, new_tip = 'never', trash = False)
            p300.drop_tip()
        magdeck.disengage()

    protocol.pause("refill tip racks")
    p300.reset_tipracks()

    protocol.comment('Warming tempdeck to 65C')
    tempdeck.set_temperature(65)
    protocol.pause('transfer onto heatblock for 30 minutes incubation\nmight have to manually pipette away remaining liquid still')
    protocol.delay(minutes = 30) 
    protocol.pause('transfer back to magplate')


    # # if i want to use the p20 and adjust its positions
    # # should also add a bit to adjust the y position here too
    # angles_s = [0.85 if ((i//8) + start_col) % 2 == 0 else -0.85 for i, col in enumerate(binding_cols)]
    # mix_locs_s = [col.bottom().move(types.Point(x=angle, y=0, z=0.8)) for col, angle in zip(binding_cols, angles_s)]


    # should this be done with p20 instead?
    for m, mix_loc in zip(binding_cols, mix_locs_m):
        p300.pick_up_tip()
        p300.transfer(
            40,
            elution_buffer,
            m,
            new_tip = 'never',
            trash = False)
        # for _ in range(10):
        #     p300.aspirate(30, m.bottom(0.2))
        #     p300.dispense(30, m.bottom(1))
        p300.move_to(m.bottom(0.2))
        p300.mix(10, 30, mix_loc, rate = 2.0)
        p300.blow_out(m.top())
        p300.drop_tip()

    protocol.pause('transfer onto heatblock and incubate for 10 minutes')
    protocol.delay(minutes = 10) 
    protocol.pause('transfer back to magplate')

    tempdeck.deactivate()
    magdeck.engage(height = magdeck_default_height)
    protocol.delay(minutes = 7)

    # fills the collection tubes by columns
    # for i in range(start_col*8, end_cols*8):
    for i in range(0, num_cols*8):
        p20.transfer(
            30,
            magdeck_plate.wells()[(start_col * 8) + i].bottom(0.2),
            elution_tubes.wells()[i],
            new_tip = 'once',
            trash = True)
    magdeck.disengage()
