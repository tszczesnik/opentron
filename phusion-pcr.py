# metadata
metadata = {
    'protocolName': 'Phusion PCR',
    'author': 'Tomasz Szczesnik <tomasz.szczesnik@bsse.ethz.ch>',
    'apiLevel': '2.6'}

def run(protocol):

    tiprack20 = protocol.load_labware('opentrons_96_tiprack_20ul', '3', '20µl tiprack')
    p20 = protocol.load_instrument('p20_single_gen2', 'left', tip_racks = [tiprack20])

    # put some ice underneath for the phusion mastermix
    tubeblock = protocol.load_labware('opentrons_24_tuberack_eppendorf_1.5ml_safelock_snapcap', '1')

    thermocycler = protocol.load_module('thermocycler')
    # should have a custom labware definition for pcr strips instead of using this nest wellplate.
    # should be ok since the xy positions and base are the same.
    thermocycler_tubes = thermocycler.load_labware('nest_96_wellplate_100ul_pcr_full_skirt', 'thermocycler plate')

    if thermocycler.lid_position != 'open':
        thermocycler.open_lid()

    thermocycler.set_block_temperature(4)

    # what to pipette where
    water_tube = 'A1'
    phusion_mastermix = 'A2'

    pcr_targets = ['A1', 'A2', 'A3']

    template_sources = [['A3'], ['A4'], ['A4']]

    # forward then reverse
    primer_sources = [
        ['B1', 'B2'],
        ['B3', 'B4'],
        ['B5', 'B4']]

    template_dilutions = ['A6', 'A7', 'A8']

    p20.pick_up_tip()
    p20.transfer(
        7,
        tubeblock[water_tube],
        [thermocycler_tubes[well_name] for well_name in pcr_targets],
        # trash = False, 
        new_tip = 'never')

    p20.transfer(
        19,
        tubeblock[water_tube],
        [thermocycler_tubes[well_name] for well_name in template_dilutions],
        # trash = False, 
        new_tip = 'never')
    # p20.return_tip()
    p20.drop_tip()

    # dilute the template and add to each well 
    for i in range(0, 3):

        p20.pick_up_tip()
        p20.transfer(
            1,
            [tubeblock.wells_by_name()[well_name] for well_name in template_sources[i]],
            thermocycler_tubes[template_dilutions[i]],
            # trash = False, 
            mix_after = (3, 20),
            new_tip = 'never')

        p20.transfer(
            1,
            # source
            thermocycler_tubes[template_dilutions[i]],
            thermocycler_tubes[pcr_targets[i]],
            # trash = False, 
            new_tip = 'never')
        # p20.return_tip()
        p20.drop_tip()

    # add the primers
    for i in range(0, 3):
        p20.transfer(
            1,
            [tubeblock.wells_by_name()[well_name] for well_name in primer_sources[i]],
            thermocycler_tubes[pcr_targets[i]],
            # trash = False, 
            new_tip = 'always')


    # add the master mix and mix
    p20.transfer(
        10,
        tubeblock[phusion_mastermix],
        [thermocycler_tubes[well_name] for well_name in pcr_targets],
        mix_after = (5, 20),
        # trash = False, 
        new_tip = 'always')

    # run the thermocycler
    if thermocycler.lid_position != 'closed':
        thermocycler.close_lid()
    thermocycler.set_lid_temperature(105)

    profile = [
        {'temperature': 98, 'hold_time_seconds': 1},
        {'temperature': 60, 'hold_time_seconds': 5},
        {'temperature': 72, 'hold_time_seconds': 10}]

    thermocycler.set_block_temperature(98, hold_time_seconds = 10, block_max_volume = 20)
    thermocycler.execute_profile(steps=profile, repetitions = 30, block_max_volume = 20)
    thermocycler.set_block_temperature(72, hold_time_seconds = 60, block_max_volume = 20)
    thermocycler.set_block_temperature(4)
