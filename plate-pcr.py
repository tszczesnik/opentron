from opentrons import types

# # metadata
# metadata = {
#     'protocolName': 'Phusion PCR',
#     'author': 'Tomasz Szczesnik <tomasz.szczesnik@bsse.ethz.ch>',
#     'apiLevel': '2.6'}

import opentrons.execute
protocol = opentrons.execute.get_protocol_api('2.9')
protocol.home()


# def run(protocol):

tipracks20 = [protocol.load_labware('opentrons_96_tiprack_20ul', i, '20ul tiprack') for i in ['6', '9']]
p20 = protocol.load_instrument('p20_single_gen2', 'left', tip_racks = tipracks20)

thermocycler = protocol.load_module('thermocycler')
# using the qpcr plates
thermocycler_tubes = thermocycler.load_labware('nest_96_wellplate_100ul_pcr_full_skirt', 'thermocycler plate')

if thermocycler.lid_position != 'open':
    thermocycler.open_lid()

thermocycler.set_block_temperature(4)


# onetaq + primer mastermix
tubeblock = protocol.load_labware('opentrons_24_tuberack_eppendorf_1.5ml_safelock_snapcap', '3')
mastermix = tubeblock['A1']
elutiontube = tubeblock['A2']

well_plate = protocol.load_labware('nest_96_wellplate_200ul_flat', '1')


# overall protol:
# fill the plate with mastermix
# transfer genomic dna from the plate
# run the pcr

all_wells = [chr(i) + str(j)
             for i in range(ord('A'), ord('A') + 8)
             for j in range(1, 13)]

p20.distribute(
    8.1,
    mastermix,
    [thermocycler_tubes[well] for well in all_wells],
    blow_out = False,
    new_tip = 'once')

# this position should be ideal for the flat cell culture 96 well plates.
well_plate_locations = [well_plate[well].bottom().move(types.Point(x=-1.8, y=0, z=-0.6))
                        for well in all_wells]

# p20.transfer(
#     6.9,
#     well_plate_locations,
#     [thermocycler_tubes[well] for well in all_wells],
#     mix_after = (3, 10),
#     # trash = False,
#     home_after = False, # otherwise wastes a lot of time after dropping each tip
#     new_tip = 'always')

for i in range(0, len(all_wells)):
    p20.pick_up_tip()

    # p20.transfer(
    #     15,
    #     elutiontube,
    #     well_plate_locations[i],
    #     mix_after = (3, 10),
    #     trash = False,
    #     new_tip = 'never')

    p20.transfer(
        6.9,
        well_plate_locations[i],
        thermocycler_tubes[all_wells[i]],
        mix_after = (3, 10),
        trash = False,
        new_tip = 'never')

    # otherwise wastes a lot of time after dropping each tip
    p20.drop_tip(home_after = False)

# protocol.pause('check everything ok before running pcr.')

# run the thermocycler
if thermocycler.lid_position != 'closed':
    thermocycler.close_lid()
thermocycler.set_lid_temperature(105)

# onetaq
profile = [
    {'temperature': 94, 'hold_time_seconds': 30},
    {'temperature': 55, 'hold_time_seconds': 30},
    {'temperature': 68, 'hold_time_seconds': 240}] # 60s per kb

thermocycler.set_block_temperature(94, hold_time_seconds = 240, block_max_volume = 20)
thermocycler.execute_profile(steps=profile, repetitions = 35, block_max_volume = 20)
thermocycler.set_block_temperature(68, hold_time_seconds = 300, block_max_volume = 20)
thermocycler.set_block_temperature(4)
thermocycler.deactivate_lid()

